var gulp = require('gulp');

var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');


var data = require('gulp-data');
var pug = require('gulp-pug');
var path = require('path');
var fs = require('fs');


gulp.task('sassBuild', function(){
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'));
});

gulp.task('fontsBuild', function(){
    return gulp.src('./src/fonts/**/*.*')
        .pipe(gulp.dest('./dist/fonts'))
});

gulp.task('pugBuild', function buildHTML(){
    return gulp.src('./src/pug/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        } ))
        .pipe(
        pug({
            pretty: true
        })
    )
        .pipe(gulp.dest('./dist'));
});

var scriptsList = [
    './bower_components/jquery/dist/jquery.js',
    './bower_components/device.js/lib/device.js',
    './bower_components/responsive-bootstrap-toolkit/dist/bootstrap-toolkit.js',
    './bower_components/owl.carousel/dist/owl.carousel.js',
    './bower_components/chosen/chosen.jquery.js',
    './src/scripts/materialize/global.js',
    './src/scripts/materialize/velocity.min.js',
    './src/scripts/materialize/forms.js',
    './src/scripts/materialize/tabs.js',
    './src/scripts/nouislider.js',
    './src/scripts/masonry.pkgd.js',
    './src/scripts/headroom.js',
    './src/scripts/jQuery.headroom.js',
    './src/scripts/jquery.stickit.js',
    './src/scripts/tooltipster.bundle.js',
    './src/scripts/jquery.scrollTo.js',
    './src/scripts/perfect-scrollbar.jquery.js',
    './src/scripts/script.js'
];


gulp.task('scriptsBuild', function(){
    gulp.src(scriptsList)
        .pipe(concat('build.js'))
        .pipe(gulp.dest('./src/scripts'))
});

gulp.task('uglifyScript', function(){
    gulp.src('./src/scripts/build.js')
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'))
});

gulp.task('imageBuild', function(){
    gulp.src(['./src/images/**/*.jpg','./src/images/**/*.png'])
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/images'))
});

gulp.task('build', ['sassBuild', 'fontsBuild', 'pugBuild', 'scriptsBuild', 'uglifyScript'], function(){

});