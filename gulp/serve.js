var gulp = require('gulp');

var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');


var data = require('gulp-data');
var pug = require('gulp-pug');
var path = require('path');
var fs = require('fs');

var browserSync = require('browser-sync').create();
var reload      = browserSync.reload;

// Dev tools
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;
var _ = require('lodash');
var $ = require('gulp-load-plugins')();

function isOnlyChange(event){return event.type === 'changed'}

gulp.task('serve', ['watch'], function(){
    browserSync.init({
        server: {
            baseDir: ['./src', './temp/serve'],
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });
});

gulp.task('watch', ['inject'], function(){
    gulp.watch(['./src/pug/**/*.pug', './src/pug/data.json'], ['inject-reload']);

    gulp.watch('./src/sass/**/*.scss', function (event){
        if ( isOnlyChange(event) )
        {
            gulp.start('styles-reload');
        }
        else
        {
            gulp.start('inject-reload');
        }
    });
    gulp.watch('./src/scripts/**/*.js', function (event)
    {
        if ( isOnlyChange(event) )
        {
            gulp.start('scripts-reload');
        }
        else
        {
            gulp.start('inject-reload');
        }
    });
});


gulp.task('inject', ['styles','pug'], function ()
{
    var injectStyles = gulp.src('./temp/serve/css/*.css', {read: false});

    var injectScripts = gulp.src([
        './src/scripts/materialize/global.js',
        './src/scripts/materialize/velocity.min.js',
        './src/scripts/materialize/hammer.min.js',
        './src/scripts/materialize/jquery.hammer.js',
        './src/scripts/materialize/forms.js',
        './src/scripts/materialize/tabs.js',
        './src/scripts/materialize/sideNav.js',
        './src/scripts/masonry.pkgd.js',
        './src/scripts/nouislider.js',
        './src/scripts/headroom.js',
        './src/scripts/jQuery.headroom.js',
        './src/scripts/jquery.stickit.js',
        './src/scripts/tooltipster.bundle.js',
        './src/scripts/jquery.scrollTo.js',
        './src/scripts/perfect-scrollbar.jquery.js',
        './src/scripts/script.js'
    ], {read: false});

    var injectOptions = {
        ignorePath  : ['/src','/temp/serve'],
        addRootSlash: false
    };

    return gulp.src('./temp/pug/*.html')
        .pipe($.inject(injectStyles, injectOptions))
        .pipe($.inject(injectScripts, injectOptions))
        .pipe(wiredep(_.extend({}, './bower_components')))
        .pipe(gulp.dest('./temp/serve'));
});


gulp.task('inject-reload', ['inject'], function ()
{
    browserSync.reload();
});

gulp.task('scripts-reload', function ()
{
    browserSync.reload();
});

var buildStyles = function () {

    return gulp.src('./src/sass/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./temp/serve/css/'));
};
gulp.task('styles-reload', ['styles'], function ()
{
    return buildStyles()
        .pipe(browserSync.stream());
});

gulp.task('styles', function ()
{
    return buildStyles();
});


gulp.task('pug', function buildHTML(){
    return gulp.src('./src/pug/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(fs.readFileSync('./src/pug/data.json'));
        } ))
        .pipe(
        pug({
            pretty: false
        })
    )
        .pipe(gulp.dest('./temp/pug/'));
});