var func = {}

;(function () {
    var site = {
        menu: $('.menu'),
        mainGallery: $('.mainGallery'),
        owlCarousel: $('.owl-carousel'),
        selectTag: $('select'),
        spinner: $('.u-spinner'),
        instruction: $('.instruction'),
        priceRange: $('.price-range'),
        filter: $('.filter'),
        cardSlider: $('.card__slider'),
        fullFeedback: $('#fullFeedback'),
        navInner: $('.navInner'),
        headerScroll: $('.headerScroll'),
        sticky: $('#sticky'),
        picker: $('.picker'),
        separator: $('.separator'),
        hero: $('.hero')
    };
    func = {
        menu: function () {
            var item = site.menu.find('.menu__item');
            item.each(function () {
                var _ = $(this);
                if (_.hasClass('_sub')) {
                    _.hover(function () {
                        clearTimeout($.data(this, 'timer'));
                        _.addClass('_active');
                        _.find('.menu__inner').slideDown(200);
                    }, function () {
                        $.data(this, 'timer', setTimeout($.proxy(function () {
                            _.removeClass('_active');
                            _.find('.menu__inner').slideUp(200);
                        }, this), 100));
                    });
                }
            })
        },
        mainGall: function () {
            site.mainGallery.each(function () {
                var _ = $(this),
                    _left = _.find('.mainGallery__left'),
                    _right = _.find('.mainGallery__right'),
                    _carousel = _.find('.mainGallery__slider');
                _carousel.owlCarousel({
                    loop: false,
                    nav: false,
                    dots: false,
                    margin: _carousel.data('margin'),
                    mouseDrag: false,
                    responsive: {
                        0: {
                            items: 1
                        },
                        768: {
                            items: 2
                        },
                        992: {
                            items: 3
                        },
                        1220: {
                            items: 4
                        }
                    }
                });

                _left.click(function (e) {
                    e.preventDefault();
                    _carousel.trigger('prev.owl.carousel')
                });

                _right.click(function (e) {
                    e.preventDefault();
                    _carousel.trigger('next.owl.carousel')
                });
            })
        },
        mainGallTab: function () {
            site.mainGallery.each(function () {
                var _ = $(this),
                    _link = _.find('.mainGallery__link'),
                    _list = _.find('.mainGallery__slider'),
                    wrapW = 0;

                _link.click(function (e) {
                    e.preventDefault();
                    var _ = $(this),
                        _index = _.index();

                    if (!_.hasClass('_active')) {
                        _list.removeClass('_active');
                        _link.removeClass('_active');
                        _list.eq(_index).addClass('_active');
                        _link.eq(_index).addClass('_active');
                    }
                });


                if (device.tablet() || device.mobile()) {
                    _link.wrapAll("<div class='mainGallery__tabs'><div class='mainGallery__wrap'></div></div>");
                    _link.each(function () {
                        wrapW += $(this).outerWidth(true)
                    });
                    _link.parent().width(wrapW + 1);
                }
            })
        },

        spinner: function (wrap) {
            var list = $(wrap).find('.u-spinner');
            list.each(function () {
                var _ = $(this),
                    _e = _.find('.u-spinner__e'),
                    _input = _.find('.u-spinner__input');

                _e.on('click', function () {

                    if ($(this).hasClass('_left') && _input.val() > 1) {
                        _input.val(function (i, val) {
                            val--;
                            return val
                        });
                    } else if ($(this).hasClass('_right')) {
                        _input.val(function (i, val) {
                            val++;
                            return val
                        });
                    }
                })
            })
        },
        instructionsGrid: function () {
            if (device.desktop()) {
                site.instruction.masonry({
                    itemSelector: '.instruction__box',
                    columnWidth: '.instruction__box'
                });
            }
        },
        priceRange: function () {
            if (site.priceRange.length > 0) {
                site.priceRange.each(function () {
                    var _this = $(this),
                        min = parseInt(_this.data('min')),
                        max = parseInt(_this.data('max')),
                        step = parseInt(_this.data('step')),
                        start = [parseInt(_this.data('min')), parseInt(_this.data('max'))],
                        result = _this.closest('.u-range').find('.u-range__result'),
                        hidden = _this.closest('.u-range').find('.u-range__hidden');

                    noUiSlider.create(_this[0], {
                        start: start,
                        connect: true,
                        step: step,
                        range: {
                            'min': min,
                            'max': max
                        }
                    });
                    _this[0].noUiSlider.on('update', function (values, handle) {
                        result[handle].innerHTML = Math.round(values[handle]);
                        hidden[handle].value = Math.round(values[handle]);
                    });
                });

            }


        },
        filter: function () {
            var box = site.filter.find('.filter__box');

            box.each(function () {
                var _ = $(this),
                    _trigger = _.find('.filter__trigger');

                _trigger.click(function (e) {
                    e.preventDefault();
                    if (!$(this).closest(box).hasClass('_active')) {
                        $(this).addClass('_active');
                        $(this).closest(box).addClass('_active');
                        $(this).closest(box).find('.filter__body').addClass('_active').slideDown(200);
                    } else {
                        $(this).removeClass('_active');
                        $(this).closest(box).removeClass('_active');
                        $(this).closest(box).find('.filter__body').removeClass('_active').slideUp(200);
                    }

                })
            })
        },
        cardSlider: function () {
            if (site.cardSlider.length > 0) {

                var _big = site.cardSlider.find('.card__pic'),
                    _thumbinail = site.cardSlider.find('.cardThumb'),
                    _thumbSlider = _thumbinail.find('.cardThumb__slider'),
                    _ev = _thumbinail.find('.cardThumb__e');
                if (_big.children().length > 1) {
                    _big.addClass('owl-carousel');
                }
                if (_thumbSlider.children().length > 3) {
                    _thumbSlider.addClass('owl-carousel');
                    _thumbSlider.owlCarousel({
                        items: 3,
                        loop: true,
                        mouseDrag: false,
                        margin: 10,
                        nav: false,
                        center: true
                    });
                    _big.owlCarousel({
                        items: 1,
                        loop: true,
                        //mouseDrag: false,
                        dots: false,
                        nav: false
                    });

                } else if (_thumbSlider.children().length > 1 && _thumbSlider.children().length < 4) {
                    _thumbinail.addClass('_minimal');
                    _thumbSlider.addClass('owl-carousel');
                    _thumbSlider.owlCarousel({
                        items: _thumbSlider.children().length,
                        loop: false,
                        mouseDrag: false,
                        margin: 10,
                        nav: false,
                        center: true
                    });
                    _big.owlCarousel({
                        items: 1,
                        loop: false,
                        //mouseDrag: false,
                        dots: false,
                        nav: false
                    });
                } else {
                    _thumbinail.hide();
                }

                _ev.click(function (e) {
                    e.preventDefault();
                    if ($(this).hasClass('_left')) {
                        _thumbSlider.trigger('prev.owl.carousel');
                        _big.trigger('prev.owl.carousel');
                    } else if ($(this).hasClass('_right')) {
                        _thumbSlider.trigger('next.owl.carousel');
                        _big.trigger('next.owl.carousel');
                    }
                })


            }
        },
        fullFeedback: function () {
            if (device.desktop() && site.fullFeedback) {
                $(window).load(function () {
                    site.fullFeedback.masonry({
                        itemSelector: '.fx-md-3',
                        columnWidth: '.fx-md-3'
                    });
                })
            }
        },
        navInnerHeight: function () {
            if (site.navInner) {
                var _items = site.navInner.find('.navInner__separator'),
                    maxH = 0;

                _items.each(function () {
                    if ($(this).outerHeight() > maxH) maxH = $(this).outerHeight()
                });
                _items.height(maxH);
            }
        },
        headerScroll: function () {
            if (site.navInner && device.desktop()) {
                var _items = site.navInner.find('.navInner__separator'),
                    maxH = 0;

                _items.each(function () {
                    if ($(this).outerHeight() > maxH) maxH = $(this).outerHeight()
                });
                _items.height(maxH);
            }
        },
        stickySidebar: function () {
            if (site.sticky && device.desktop()) {
                site.sticky.stickit();
            }
        },
        headerPopup: function () {
            var _item = $('.popup__trigger');

            _item.each(function(){
                var _ = $(this),
                    _popup = _.siblings('.popup__drop');
                _.on('mouseenter', function () {
                    _popup.addClass('_active');
                });
                _.on('mouseleave', function () {
                    var timer = setTimeout(function () {
                        _popup.removeClass('_active')
                    }, 300);
                    _popup.on('mouseenter', function () {
                        $(this).addClass('_active');
                        clearTimeout(timer);
                    });
                });
                _popup.on('mouseleave', function () {
                    $(this).removeClass('_active');
                })
            });
        },
        stickyHead: function () {
            if (device.desktop()) {
                site.headerScroll.headroom();
            }
        },
        picker: function () {
            var _handler = site.picker.find('.picker__e'),
                _tabs = site.picker.find('.picker__item'),
                _wrap = site.picker.find('.picker__wrap');

            _handler.on('click', function () {
                var _;
                if ($(this).hasClass('_left') && $(this).siblings('.picker__input').val() > 1) {
                    $(this).siblings('.picker__input').val(function (i, val) {
                        val--;
                        return val
                    });
                    _ = $(this).siblings('.picker__input').val();
                    console.log(_);

                    $(this).parent().next().find('.picker__slide').removeClass('_active');
                    $(this).parent().next().find('.picker__slide').eq(_ - 1).addClass('_active');
                } else if ($(this).hasClass('_right') && $(this).siblings('.picker__input').val() < $(this).siblings('.picker__length').val()) {
                    $(this).siblings('.picker__input').val(function (i, val) {
                        val++;
                        return val
                    });

                    _ = $(this).siblings('.picker__input').val();

                    $(this).parent().next().find('.picker__slide').removeClass('_active');
                    $(this).parent().next().find('.picker__slide').eq(_ - 1).addClass('_active');
                }
            });

            _tabs.on('click', function () {
                if (!$(this).hasClass('_active')) {
                    var _data = $(this).data('tab');
                    _tabs.removeClass('_active');
                    $(this).addClass('_active');

                    _wrap.removeClass('_active');
                    _wrap.filter(function () {
                        return $(this).data('container') == _data
                    }).addClass('_active')
                }
            })
        },
        separatorSlider: function () {
            var _slider = site.separator.find('.separator__slider'),
                _handler = site.separator.find('.separator__e');


            _slider.owlCarousel({
                items: 1,
                loop: true,
                mouseDrag: false,
                dots: false,
                nav: false
            });

            _handler.on('click', function () {
                if ($(this).hasClass('_left')) {
                    _slider.trigger('prev.owl.carousel')
                } else {
                    _slider.trigger('next.owl.carousel')
                }
            })
        },
        hero: function () {
            var _btn = site.hero.find('.hero__btn');

            _btn.on('click', function () {
                var _target = $(this).data('target');
                $.scrollTo(_target, 500, {
                    offset: {
                        left: 0,
                        top: -65
                    }
                });
            })
        },
        customSelect: function(wrap){
            wrap.find(".chosen-select").chosen({
                disable_search_threshold: 10
            });
        },
        mainGallTabAjax: function () {
            $('.ajaxTabs').each(function(){
                var _ = $(this),
                    _link = _.find('.ajaxTabs__link'),
                    _content = _.find('.ajaxTabs__content'),
                    _body = _.find('.ajaxTabs__body'),
                    _left = _.find('.ajaxTabs__left'),
                    _right = _.find('.ajaxTabs__right'),
                    _carousel,
                    _bodyH = 0,
                    wrapW = 0;

                var _load = function(height){
                    var hash = this.hash.substr(1);
                    if (height){
                        _body.height(height);
                    }

                    _content.html('<div class="ajaxTabs__preloader"></div>');
                    $.get(hash, function(data){
                        _content.html(data);
                        func.spinner(_content);
                        func.customSelect(_content);
                        _carousel = _content.find('.ajaxTabs__slider');
                        _carousel.owlCarousel({
                            loop: true,
                            nav: false,
                            dots: false,
                            mouseDrag: false,
                            margin: _carousel.data('margin'),
                            responsive: {
                                0: {
                                    items: 1
                                },
                                768: {
                                    items: 2
                                },
                                992: {
                                    items: 3
                                },
                                1220: {
                                    items: 4
                                }
                            }
                        });
                        $('.gallery__item').click(function(){
                            console.log(123)
                        })
                    });

                };


                _link.click(function (e) {
                    e.preventDefault();
                    _bodyH = _content.outerHeight();
                    if (!$(this).hasClass('_active')){
                        _link.removeClass('_active');
                        $(this).addClass('_active');
                        _load.call(this, _bodyH);
                    }

                });
                _link.each(function(){
                    if ($(this).hasClass('_active')){
                        _load.call(this);
                    }
                });
                _left.on('click', function (e) {
                    e.preventDefault();
                    _carousel.trigger('prev.owl.carousel')
                });

                _right.on('click', function (e) {
                    e.preventDefault();
                    _carousel.trigger('next.owl.carousel')
                });




                if (device.tablet() || device.mobile()) {
                    _link.wrapAll("<div class='mainGallery__tabs'><div class='mainGallery__wrap'></div></div>");
                    _link.each(function () {
                        wrapW += $(this).outerWidth(true)
                    });
                    _link.parent().width(wrapW + 1);
                }
            });
        }
    };
    $(document).ready(function () {
        func.menu();
        func.mainGall();
        //func.mainGallTab();
        func.mainGallTabAjax();
        //func.spinner();
        func.filter();
        func.cardSlider();
        func.instructionsGrid();
        func.priceRange();
        func.fullFeedback();
        func.navInnerHeight();
        func.stickySidebar();
        func.headerPopup();
        func.stickyHead();
        func.picker();
        func.separatorSlider();
        func.hero();
        //func.customSelect();


        site.owlCarousel.owlCarousel({
            items: 1,
            nav: false
        });

        $('.tooltip').tooltipster({
            theme: 'myTheme',
            trigger: 'click'
        });

        if ($('.dropcart__wrap').length > 0){
            $('.dropcart__wrap').perfectScrollbar();
        }





        if ($('.step__select').length > 0){
            func.customSelect($('.step__select'));
        }

        if ($('.showcase__select').length > 0){
            func.customSelect($('.showcase__select'));
        }
        if ($('.showcase__input').length > 0){
            func.spinner($('.showcase__input'))
        }
        $(window).load(function(){
            console.log($('.minicart').offset());
        });
        //$('ul.tabs').tabs();


        $('.openFilter').sideNav({
            edge: 'right'
        })
    })
}());